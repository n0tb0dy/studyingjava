package pro.java.priv;
import static pro.java.util.Print.*;

public class Root {
	private void prt() { println("Root"); }
	public static void main(String[] args) {
		Root root = new Branch();
		root.prt();
	}
}

class Branch extends Root{
	void prt() { println("Branch");	}
}