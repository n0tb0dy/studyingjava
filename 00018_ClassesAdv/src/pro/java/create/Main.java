package pro.java.create;
import static pro.java.util.Print.*;

class MyString {
	String myStr;

	MyString(String s) {
		println("MyString constructor s = " + s);
		myStr = s;
	}
	public String toString() { return myStr; }
}

class Sup {
	{ println("SUP инициализационный блок 1"); }
	String set = setSup();
	{ println("SUP инициализационный блок 2"); }
	Sup() {
		println("SUP() перед вызовом display()");
		display();
		println("SUP() после вызова display()");
		println("SUP set = " + set );
	}
	void display() { println("!!!SUP set = " + set); }
	String setSup(){
		println("SUP setSup()");
		return "MySuperString";
	}
}

class Sub extends Sup{
	{ println("Sub инициализационный блок 1"); }
	String set = setSub();
	{ println("Sub инициализационный блок 2"); }
	Sub() {
		println("Sub() перед вызовом display()");
		display();
		println("Sub() после вызова display()");
	}
	void display() { println("Sub set = " + set); }
	String setSub(){
		println("Sub setSub()");
		return "MySubString";
	}
}

public class Main {

	public static void main(String[] args) {
		new Sub();
	}
}
