package pro.java.shapes03;

import static pro.java.util.Print.*;

class Shape {
	public String toString() { return "Shape"; }
}

class Circle extends Shape {
	public String toString() { return "Circle"; }
}

class BuildShape {
	Shape build() { return new Shape(); }
}

class BuildCircle extends BuildShape {
	@Override
	Circle build() { return new Circle(); }
}

public class Main {
	public static void main(String[] args) {
		Shape s = new BuildShape().build();
		println(s);
		s = new BuildCircle().build();
		println(s);
	}
}
