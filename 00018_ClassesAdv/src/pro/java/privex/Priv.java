package pro.java.privex;
import static pro.java.util.Print.*;

public class Priv {

	public static void main(String[] args) {
		B b = new B();
		b.prt();
	}
}

class A {
	private String stra = "PRIVATE";
	private void privprint() {println(stra);}
	protected void prt() {privprint();};
}

class B extends A {}