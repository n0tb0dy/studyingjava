package pro.java.abs;
import static pro.java.util.Print.*;

public class MainAbs {
	public static void main(String[] args) {
		AbsExample.somePrint("Static Abstract Print");
		Example ex = new Example();
		ex.somePrint("Extended Print");
	}
}

abstract class AbsExample {
	static void somePrint(Object obj) {
		println(obj);
	}

	abstract void someAbsPrint(Object obj);
}

class Example extends AbsExample {
	@Override
	void someAbsPrint(Object obj) {
		println(obj);
	}
}
