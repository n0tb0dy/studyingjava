import static pro.java.util.Print.*;

public class Array10 {

	public static void main(String[] args) {
		// Демонстрация трехмерного массива
		int threeD[][][] = new int[3][4][5];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 5; k++) {
					threeD[i][j][k] = i * j * k;
					print(threeD[i][j][k] + " ");
				}
				println();
			}
			println();
		}
	}
}
