import static pro.java.util.Print.*;
import java.util.Arrays;

public class Array13 {

	public static void main(String[] args) {
		// Примеры работы с массивами
		
		int myMaxOld=max_old(new int[]{5,8,9,7});
		println("myMaxOld = "+myMaxOld);
		
		//использум в вызове синтаксис Varargs
		int myMaxNew=max_new(5,8,3,7);
		println("myMaxNew = "+myMaxNew);
	}
	
	private static int max_old(int[] numbers){
		if (numbers.length==0) return 0;
		Arrays.sort(numbers);
		return numbers[numbers.length-1];
	}
	
	//использум в объявлении синтаксис Varargs
	private static int max_new(int... numbers){
		if (numbers.length==0) return 0;
		Arrays.sort(numbers);
		return numbers[numbers.length-1];
	}
}
