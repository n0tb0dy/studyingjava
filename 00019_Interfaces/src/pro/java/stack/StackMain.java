package pro.java.stack;

import static pro.java.util.Print.*;

public class StackMain {

	public static void main(String[] args) {

		IntStack fs = new FixedStack(3);
		fs.push(5);
		fs.printStack();
		println();
		fs.push(9);
		fs.printStack();
		fs.push(7);
		fs.push(3); // это не поместится в стек
		println();
		fs.printStack();

		println("Извлекли из стека " + fs.рор());
		fs.printStack();

		IntStack ds = new DynStack(2);
		ds.push(33);
		println();
		ds.printStack();
		ds.push(55);
		ds.push(77);
		ds.printStack();
		println("Извлекли из стека " + ds.рор());
		ds.printStack();

	}

}
