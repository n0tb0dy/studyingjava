package pro.java.intr01;

class Cat implements Sound {

	String type;

	Cat() {
		type = "Кошка";
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void getSound() {
		System.out.println("Мяу-мяу");
	}

}
