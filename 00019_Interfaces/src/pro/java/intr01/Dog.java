package pro.java.intr01;

class Dog implements Sound {

	String type;

	Dog() {
		type = "Собака";
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void getSound() {
		System.out.println("Гав-гав");
	}

}
