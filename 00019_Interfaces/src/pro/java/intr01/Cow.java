package pro.java.intr01;

class Cow implements Sound {
	
	String type;
	
	Cow(){
		type = "Корова";
	}

	@Override
	public String getType() {
		return type;
	}
	
	@Override
	public void getSound(){
		System.out.println("Му-му");
	}
	
}
