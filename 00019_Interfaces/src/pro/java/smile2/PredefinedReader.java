package pro.java.smile2;

public class PredefinedReader implements IReader {

	final String text;

	public PredefinedReader(String text) {
		this.text = text;
	}

	@Override
	public String read() {
		return text;
	}

}
