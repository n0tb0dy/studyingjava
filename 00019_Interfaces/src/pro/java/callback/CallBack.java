package pro.java.callback;

import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class CallBack {

	public static void main(String[] args) {
		ActionListener listener = new TimePrinter();
		Timer t = new Timer(2000, listener);
		t.start();
		JOptionPane.showMessageDialog(null, "Завершить программу?");
		System.exit(0);

	}

}
