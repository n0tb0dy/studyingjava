package pro.java.minmax;

import static pro.java.util.Print.*;

public class MaxMinDemo {

	public static void main(String[] args) {

		MaxMin mm = new MaxMin();
		println("getMax(10, 5) = " + mm.getMax(10, 5));
		println("getMin(10, 5) = " + mm.getMin(10, 5));
	}

}
