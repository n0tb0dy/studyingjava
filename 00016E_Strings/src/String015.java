import static pro.java.util.Print.*;

public class String015 {

	public static void main(String[] args) {
		// примеры сравнения строк

		String abc = "abc";
		String def = "def";
		String abc0 = "abc0";

		println("abc.compareTo(def) = " + abc.compareTo(def));
		println("abc.compareTo(abc) = " + abc.compareTo("abc"));
		println("def.compareTo(abc) = " + def.compareTo(abc));
		println("abc.compareTo(abc0) = " + abc.compareTo(abc0));

	}

}
