import static pro.java.util.Print.*;

public class String001 {

	public static void main(String[] args) {
		// строковые литералы в Java это тоже объекты

		println("Hello World!");
		println("Строка \"Hello World!\" содержит " + "Hello World!".length()
				+ " символов");
		println("Первый символ строки \"Hello World!\" это буква "
				+ "Hello World!".charAt(0));
		println("Hello World!".subSequence(0, 6));
	}
}
