import static pro.java.util.Print.*;

public class QuickComparison {
	// метод compare1
	static boolean compare1(int val) {
		print("compare1 (" + val + ") ");
		println("результат = " + (val < 7) + " потому что " + val + "<7 это "+(val<7));
		return val < 7;
	}

	// метод compare2
	static boolean compare2(int val) {
		print("compare2 (" + val + ") ");
		println("результат = " + (val > 7) + " потому что " + val + ">7 это "+(val>7));
		return val > 7;
	}

	// примеры использования условных операторов && ||
	public static void main(String[] args) {
		// метод compare2 выполнится
		println("\nУсловное И &&");
		boolean a = compare1(5) && compare2(3);
		println("a=" + a);
		println("--------------------------------");
		// метод compare2 не выполнится
		a = compare1(9) && compare2(9);
		println("a=" + a);
		println("--------------------------------");
		println("\nУсловное ИЛИ ||");
		a = compare1(9) || compare2(3);
		println("a=" + a);
		println("--------------------------------");
		a = compare1(3) || compare2(9);
		println("a=" + a);
	}
}
