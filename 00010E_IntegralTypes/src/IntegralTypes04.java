import static pro.java.util.Print.*;

public class IntegralTypes04 {

	public static void main(String[] args) {
		// Примеры граблей при операциях сдвига
		int mask = 0xFF000000;
		print("mask = ");
		printlnInt(mask);
		println("mask >>> 16 " + "16%32=" + (16 % 32));
		print("mask = ");
		printlnInt((mask >>> 16));
		println("mask >>> 24 " + "24%32=" + (24 % 32));
		print("mask = ");
		printlnInt((mask >>> 24));
		println("mask >>> 32 " + "32%32=" + (32 % 32));
		print("mask = ");
		printlnInt((mask >>> 32)); // сдвига не произойдет
		println("mask >> 32 " + "32%32=" + (32 % 32));
		print("mask = ");
		printlnInt((mask >> 32)); // сдвига не произойдет
		println("mask << 32 " + "32%32=" + (32 % 32));
		print("mask = ");
		printlnInt((mask << 32)); // сдвига не произойдет
		println("mask << 64 " + "64%32=" + (64 % 32));
		print("mask = ");
		printlnInt((mask << 64)); // сдвига не произойдет
		println("mask >>> 48 " + "48%32=" + (48 % 32));
		print("mask = ");
		printlnInt((mask >>> 48)); //сдвиг на 16 вправо
		
	}
}
