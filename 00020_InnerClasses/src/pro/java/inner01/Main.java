package pro.java.inner01;

import static pro.java.util.Print.*;

public class Main {

	public static void main(String[] args) {
		// Пример использования inner классов
		OuterClass outClass = new OuterClass();
		OuterClass.InnerClass ic1 = outClass.getInnerClass(1);
		OuterClass.InnerClass ic2 = outClass.getInnerClass(2);
		println("ic1.getInnerInt = " + ic1.getInnerInt());
		println("ic2.getInnerInt = " + ic2.getInnerInt());
		println("ic1.getOutInt = " + ic1.getOutInt());
		println("ic2.getOutInt = " + ic2.getOutInt());
		

	}

}
