package pro.java.human;

import static pro.java.util.Print.*;

public class HumanUsage {

	public static void main(String[] args) {
		
		// создали объект вложенного класса Relations
		Human.Relations hr = new Human.Relations("married");
		
		// создали объект внешненго класса Human
		Human h = new Human();
		
		// присвоили объект hr полю relations обекта h
		h.relations = hr;
		
		println(h.relations.getRelatons());
		
	}

}
