package pro.java.local01;

import static pro.java.util.Print.*;

class External01 {
	String strE01 = "External01";
}

class External02 {
	String strE02 = "External02";
}

public class Outer {
	String strO = "Outer";
	private String strOP = "private Outer";

	class Inner extends External01 {
		String strI = "Inner";
		private final String strIP = "private final Inner";

		void printInner(String afStr) {
			final String fstr = "printLocal final";
			String noFinal = "noFinal";

			class Local extends External02 {
				String strL = "Local";

				void printLocal() {
					println("strL = " + strL);
					println("fstr = " + fstr);
					println("noFinal = " + noFinal);
					println("afStr = " + afStr);
					println("strI = " + strI);
					println("strIP = " + strIP);
					println("str0P = " + strOP);
					println("strE02 = " + strE02);
					println("strE01 = " + strE01);
				}
			}

			Local loc = new Local();
			loc.printLocal();
		}

	}

}
