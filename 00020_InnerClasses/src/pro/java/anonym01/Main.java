package pro.java.anonym01;

import static pro.java.util.Print.*;

interface Iout { void justPrint(); }

class External {
	String str = "External";
	void extPrint() { println("ExtPrint"); } 
}

class Outer {
	String str = "Outer";

	void outPrint() {
		Iout iout = new Iout() {
			public void justPrint() {
				println("Just Print!");
			}
		};
		iout.justPrint();
	}
	
	void extPrint() {
		External ext = new External(){
			String str = "Anonymous";
			void extPrint() {
				println("AnonymPrint");
				super.extPrint();
				println("str = " + str);
				println("super.str = " + super.str);
				println("Outer.this.str = " + Outer.this.str);
			}
		};
		ext.extPrint();
	}
}

public class Main {
	public static void main(String[] args) {
		Outer out = new Outer();
		out.outPrint();
		out.extPrint();
	}
}
