package pro.java.anonym03;

import static pro.java.util.Print.*;

abstract class Base {
	private static int i;

	Base(int i) {
		println("Base constructor, i= " + i);
		Base.i = i + 10;
		println("Base constructor, Base.i= " + Base.i);
	};

	public abstract void f();

	public static Base getAnonBase(int i) {
		return new Base(i) {
			private int anonI;

			// инициализатор анонимного класса
			{
				anonI = i + 20;
				println("Инициализатор анонимного класса i = " + i);
				println("Инициализатор анонимного класса anonI = " + anonI);
				println("Инициализатор анонимного класса Base.i = " + Base.i);
			}

			public void f() {
				println("Метод f() анонимного класса i = " + i);
				println("Метод f() анонимного класса anonI = " + anonI);
				println("Метод f() анонимного класса Base.i = " + Base.i);
				// i++; // если раскоментировать то будет ошибка
			}
		};
	}
}

public class Main {

	public static void main(String[] args) {

		Base anon = Base.getAnonBase(10);
		anon.f();

	}

}
