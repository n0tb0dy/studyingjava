package pro.java.nestest;

public class SomeClass {
	
	void printMyString() {
		System.out.println("MyString");
	}
	
	public static class Test {
		public static void main(String[] args) {
			SomeClass test = new SomeClass();
			test.printMyString();
		}
		
	}

}
