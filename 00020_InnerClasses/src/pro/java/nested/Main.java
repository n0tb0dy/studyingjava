package pro.java.nested;

import static pro.java.util.Print.println;

import pro.java.nested.Outer.IGetNames.GetNames;

public class Main {

	public static void main(String[] args) {
		// Пример использования nested классов

		// создали экземпляр вложенного класса 
		// без создания экземпляра внешнего класса
		Outer.Nested nst = new Outer.Nested("MyNested");
		println(nst.getName());
		println(nst.getOuterName());
		println(Outer.out);
		println(Outer.Nested.nst);
		println("-------");
		
		// создали экземпляр внешнего класса
		Outer outer = new Outer();
		println(outer.getName());
		println(outer.getStnst());
		println("-------");
		
		// создали экземпляр вложеннго класса GetNames
		GetNames gn = new GetNames();
		println(gn.getOuterName());
		println(gn.getNestedStnst());
		println(gn.getNestedName(nst));

	}

}
