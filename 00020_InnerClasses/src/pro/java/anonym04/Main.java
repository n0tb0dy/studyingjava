package pro.java.anonym04;

import static pro.java.util.Print.println;

public class Main {

	public static void main(String[] args) {
		Base b = new Base("Base1");
		Base a1 = b.getAnonBase("Anon b a1");
		println("b.getThis = " + b.getThis());
		println("a1.getThis = " + a1.getThis());
		println("-------");
		External e = new External();
		println("e.getThis = " + e.getThis());
		println("e.getStr = " + e.getStr());
		println("-------");
		Base a2 = e.getAnonBase("Anon e a2");
		println("a2.getThis = " + a2.getThis());

	}

}
