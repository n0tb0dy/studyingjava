import static pro.java.util.Print.*;

public class Operators01 {

	public static void main(String[] args) {
		// Простые примеры использования некоторых операторов

		println("Total: " + 3 + 4); // Отображает "Total: 34", не 7!

		// Отображает "Quotient: 2.3333333"
		println("Quotient: " + 7 / 3.0f);

		String sYes="присутствуют.";
		String sNo="отсутствуют.";
		String isArgs = ( args.length > 0) ? sYes : sNo;
		println("Аргументы в командной строке "+isArgs);

		println("Передано аргументов в командной строке: " + args.length);
		String sArgs = null;
		sArgs = (args.length != 0) ? args[0] : "unknown";
		println("name = " + sArgs);
	}
}
