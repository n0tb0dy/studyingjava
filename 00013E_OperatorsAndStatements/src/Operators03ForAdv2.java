import static pro.java.util.Print.*;

public class Operators03ForAdv2 {

	static int i;

	public static void initTest() {
		println("Инициализация i = " + i);
	}

	public static boolean condTest() {
		println("i = " + i + " поэтому i < 4 это " + (i < 4 ? true : false));
		return i < 4 ? true : false;
	}

	public static void recTest() {
		println("Повторение. До увеличения i = " + i++);
		println("Повторение. После увеличения i = " + i);
	}

	public static void main(String[] args) {

		for (initTest(); condTest(); recTest())
			System.out.println("Тело цикла");

	}

}
