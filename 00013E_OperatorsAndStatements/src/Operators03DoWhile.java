import static pro.java.util.Print.*;

public class Operators03DoWhile {

	public static void main(String[] args) {
		// Пример оператора do wile
		int i = args.length;
		do {
			if (i == 0) break; // если нет аргументов то выходим из цикла
			--i;
			println("Привет " + args[i]);

		} while (i > 0);

		int n = 10;
		do {
			println("--> " + n);
		} while (--n > 0);

	}
}
