import static pro.java.util.Print.*;

public class Operators03ForAdv {

	public static void main(String[] args) {
		// пример продвинутого оператора for

		int i = 0;
		for (boolean b = args.length > 0; b; println("Итерация " + i
				+ " завершена")) {
			println("\nПривет " + args[i]);
			b = args.length > ++i;
		}

		// пример пустых частей оператора for
		boolean done = false;
		println();
		for (; !done;) {
			println("i равно " + i);
			if (i == 3)
				done = true;
			i++;
		}
	}
}
