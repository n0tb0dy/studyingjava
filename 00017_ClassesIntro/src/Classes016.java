import static pro.java.util.Print.*;
public class Classes016 {

	public static void main(String[] args) {
		A a = new A();
		// a.prtprv(); не возможно вызвать private метод
		// a.stra = "New"; не возможно получить доступ к private полю
		a.prtStrA();
		a.setStrA("Class A new String");
		a.prtStrA();
	}
}

class A {
	private String stra ="Class A";
	private void prtprv() {println(stra);}
	void setStrA (String str) {stra=str;}
	void prtStrA () {prtprv();}
}
