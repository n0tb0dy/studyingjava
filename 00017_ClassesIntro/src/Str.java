import static pro.java.util.Print.*;
import static pro.java.util.Sys.iHash;

public class Str {

	String myStr;

	Str(String str) {
		myStr = str;
	}

	void setStr(String str) {
		myStr = str;
	}

	void printStrHash() {
		println("хэш объекта myStr = " + iHash(myStr) + "  строка = " + myStr);
	}
	
	String getStr(){
		return myStr;
	}

}
