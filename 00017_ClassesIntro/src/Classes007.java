import static pro.java.util.Print.*;
import static pro.java.util.Sys.*;

public class Classes007 {

	static void сhange(int i) {
		println("В методе change i = " + (++i));
	}

	static void сhange(Box box1) {
		println("В методе change box1 iHash = " + iHash(box1));
		println("В методе change box1 = " + box1);
		box1.setData("BOX1");
	}

	static void chnge(Box box2) {
		println("В методе chhge до new box2 iHash  = " + iHash(box2));
		println("В методе chhge до new box2 = " + box2);
		box2 = new Box();
		println("В методе chhge после new box2 iHash  = " + iHash(box2));
		println("В методе chhge после new box2 = " + box2);
		box2.setData("NEW BOX");
	}

	public static void main(String[] args) {
		// примеры передачи аргументов в методы

		int i = 10;
		println("До вызова метода i = " + i);
		сhange(i);
		println("После вызова метода i = " + i);
		println();

		Box box1 = new Box();
		box1.setData("box1");
		println("До вызова метода box1.label = " + box1.label);
		println("До вызова метода box1 iHash = " + iHash(box1));
		println("До вызова метода box1 = " + box1);
		println();
		сhange(box1);
		println();
		println("После вызова метода box1.label = " + box1.label);
		println("После вызова метода box1 iHash = " + iHash(box1));
		println("После вызова метода box1 = " + box1);
		println();

		Box box2 = new Box();
		box2.setData("box2");
		println("До вызова метода box2.label = " + box2.label);
		println("До вызова метода box2 iHash = " + iHash(box2));
		println("До вызова метода box2 = " + box2);
		println();
		chnge(box2);
		println();
		println("После вызова метода box2.label = " + box2.label);
		println("После вызова метода box2 iHash = " + iHash(box2));
		println("После вызова метода box2 = " + box2);
	}

}
