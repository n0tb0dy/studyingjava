import static pro.java.util.Print.*;
public class JavaDataTypes {

	public static void main(String[] args) {
		println("Пример примитивных и ссылочных типов данных в Java");
		println();
		println("Примитивные типы");
		//переменные примитивных типов передаются по значению
		int i=4;
		int j=i;
		println("i="+i+"   j="+j);
		++j;
		println("После инкремента ++j");
		println("i="+i+"   j="+j);
		
		//переменные ссылочных типов передаются по ссылке
		println();
		println("Ссылочные типы");
		Dog thisDog=new Dog("Sparky");
		Dog thatDog=thisDog;
		println("thisDog="+thisDog.GetName()+"   thatDog="+thatDog.GetName());
		println("thisDog="+thisDog+"   thatDog="+thatDog);
		println();
		thatDog.SetName("Wolfie");
		println("После присвоения thatDog Wolfie");
		println();
		println("thisDog="+thisDog.GetName()+"   thatDog="+thatDog.GetName());
		println("thisDog="+thisDog+"   thatDog="+thatDog);
	}
}
