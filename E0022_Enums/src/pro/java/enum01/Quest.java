package pro.java.enum01;

import static pro.java.util.Numbers.randomInRange;
import static pro.java.util.Print.*;
import static pro.java.enum01.SharedConstants.*;

public class Quest {
	public static void main(String[] args) {
		for (SharedConstants scont : SharedConstants.values()) {
			println("Тип:" + scont.getDeclaringClass() + 
					" Числовое значение: " + scont.ordinal() + 
					" Имя: " + scont.name());
		}
		println();
		answer(ask());
		answer(ask());
		answer(ask());
		answer(ask());
	}
	
	static void answer(SharedConstants result) {
		switch (result) {
		case NO:
			System.out.println("Нет");
			break;
		case YES:
			System.out.println("Да");
			break;
		case MAYBE:
			System.out.println("Возможно");
			break;
		case LATER:
			System.out.println("Позднее");
			break;
		case SOON:
			System.out.println("Вскоре");
			break;
		case NEVER:
			System.out.println("Никогда");
			break;
		}

	}
	
	// статический импорт класса SharedConstants
	// нужен только для этого метода
	static SharedConstants ask() { 
		int prob = randomInRange(0, 100);

		if (prob < 30)
			return NO; // 30%
		else if (prob < 60)
			return YES; // 30%
		else if (prob < 75)
			return LATER; // 15%
		else if (prob < 98)
			return SOON; // 13%
		else
			return NEVER; // 2%
	}

}
