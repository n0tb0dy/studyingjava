import static pro.java.util.Print.*;
import static pro.java.util.Sys.*;

public class CompareWrapper {

	public static void main(String[] args) {
		// пример на понимание разцины между примитивными и ссылочными типами
		int inta = 500;
		int intb = inta;
		println("До изменения");
		println("inta = " + inta + "  intb = " + intb);
		println("inta == intb " + (inta == intb));
		intb = 500;
		println("После изменения");
		println("inta = " + inta + "  intb = " + intb);
		print("inta == intb " + (inta == intb));
		printLnLineLn();
		
		Integer intA = 500;
		Integer intB = intA;
		println("До изменения (оператор присвавания)");
		println("intA = " + intA + "  intB = " + intB);
		println("intA == intA " + (intA == intB));
		intB = 500;
		println("После изменения");
		println("intA = " + intA + "  intB = " + intB);
		print("intA == intB " + (intA == intB));
		printLnLineLn();
		
		intA = Integer.valueOf(500);
		intB = Integer.valueOf(500);
		println("После присовения через valueOf");
		println("intA = " + intA + "  intB = " + intB);
		print("intA == intB " + (intA == intB));
		printLnLineLn();
		
		intA = new Integer(5);
		intB = new Integer(5);
		println("После присовения через new");
		println("intA = " + intA + "  intB = " + intB);
		print("intA == intB " + (intA == intB));
		printLnLineLn();
		
		print("Сравнение через equals() intA и intB дает " + intA.equals(intB));
		
		printLnLineLn();
		intA = 5;
		intB = 5;
		println("intA = " + intA + " hash = " + iHash(intA));
		println("intB = " + intB + " hash = " + iHash(intB));
		intA = 500;
		intB = 500;
		println("intA = " + intA + " hash = " + iHash(intA));
		println("intB = " + intB + " hash = " + iHash(intB));
		intA = 5;
		intB = 5;
		println("intA = " + intA + " hash = " + iHash(intA));
		println("intB = " + intB + " hash = " + iHash(intB));
		intA = 500;
		intB = 500;
		println("intA = " + intA + " hash = " + iHash(intA));
		println("intB = " + intB + " hash = " + iHash(intB));
		
	}

}
