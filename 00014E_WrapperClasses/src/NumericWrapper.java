import static pro.java.util.Print.*;

public class NumericWrapper {

	public static void main(String[] args) {
		// примеры классов-оберток для числовых типов

		println("Byte.MAX_VALUE = " + Byte.MAX_VALUE);
		println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);

		String sNum = "55"; // это строка а не число
		int a = Integer.parseInt(sNum);
		println("a = " + a);
		print(a+" в двоичном виде: ");
		printInt(a);
		println("\nbitCount in a is: "+Integer.bitCount(a));
	}
}
