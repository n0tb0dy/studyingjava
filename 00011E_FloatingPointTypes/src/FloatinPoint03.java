import static pro.java.util.Print.println;

public class FloatinPoint03 {

	public static void main(String[] args) {
		
		double dd = 3.33;
		float ff = (float)dd;
		println("dd = " + dd);
		println("ff = " + ff);
		println("ff == dd is " + (ff==dd));
		println("-------");
		dd = ff;
		println("dd = " + dd);
		println("ff = " + ff);
		println("ff == dd is " + (ff==dd));

	}

}
