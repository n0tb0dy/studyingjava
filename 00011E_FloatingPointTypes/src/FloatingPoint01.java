import static pro.java.util.Print.*;

public class FloatingPoint01 {

	public static void main(String[] args) {
		// Примеры вещественных типов в Java
		//нижняя строчка не скомпилируется
		//так как вещественыне литералы по умолчанию double
		//float fl=3.14;
		double inf = 1.0/0.0; // Бесконечность
		double neginf = -1.0/0.0; // Отрицательная бесконечность
		double negzero = -1.0/inf; // Отрицательный нуль
		double plszero = 1.0/inf; // Положительный нуль
		double NaN1 = 0.0/0.0; // Не числовое значение
		double NaN2 = 0.0*inf; // Не числовое значение
		println("inf = "+inf);
		println("neginf = "+neginf);
		println("inf == neginf is "+(inf == neginf));
		println("\nnegzero = "+negzero);
		println("plszero = "+plszero);
		println("negzero == plszero is "+(negzero == plszero));
		println("\nNaN1 = "+NaN1);
		println("NaN2 = "+NaN2);
		println("NaN1 == NaN2 is "+(NaN1 == NaN2));
		println("NaN1 != NaN2 is "+(NaN1 != NaN2));
		double de = 0.125e4; //0.125*10^4=1250.0
		println("\nde = "+de);
		double he = 0xFp2; //15*2^2=60.0
		println("he = "+he);
		double a=5.0;
		double b=3.0;
		println("\na = "+a+"    b = "+b);
		println("a / b = "+(a/b));
		println("a % b = "+(a%b));
		println("a = ((long)(a/b))*b+(a%b) = "+(((long)(a/b))*b+(a%b)));
		double sqrt=Math.sqrt(a);
		println("\nКвадратный корень из "+a+" равен "+sqrt);
	}
}
